﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenOrientation : MonoBehaviour {
    public Camera Screen1;
    public Camera Screen2;
    public MeshRenderer ScreenPosition;
    public bool Horizontal = false;


    // Use this for initialization

    public void ChangeSplitScreen () {
        Horizontal = !Horizontal;

        if(Horizontal)
        {
            Screen1.rect = new Rect(0, 0.5f, 1, 1);
            //Screen1.transform.localScale = new Vector3(2,2, 1);
            Screen2.rect = new Rect(0, 0, 1, 0.5f);
            Screen.SetResolution(256, 384, Screen.fullScreen);
            //ScreenPosition.transform.localPosition = new Vector3(0, -2.5f, 0.5f);
        }
        else
        {
            Screen1.rect = new Rect(0, 0, 0.5f, 1);
            //Screen1.transform.localScale = new Vector3(2,2, 1);
            Screen2.rect = new Rect(0.5f, 0, 0.5f, 1);
            Screen.SetResolution(512, 192, Screen.fullScreen);
            //ScreenPosition.transform.localPosition = new Vector3(0, -2.5f, 0.5f);
        }
	}
      

// Update is called once per frame
void Update ()
    {
        if (Input.GetKeyDown(KeyCode.RightControl))
            ChangeSplitScreen();
	}
}
